package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 23-Apr-17.
 */
public class UpdateOrderStatusView {

    private JButton finish = new JButton("Finish");
    private JButton cancel = new JButton("Cancel");
    private JFrame frame;
    JTextField st;

    public UpdateOrderStatusView(){

        frame = new JFrame("Update Order");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);

        JLabel status = new JLabel("Status: ");

        st = new JTextField(40);

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();

        p1.add(status);
        p1.add(st);

        p2.add(cancel);
        p2.add(finish);

        JPanel p =  new JPanel();

        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void addCancelListener(ActionListener cl){
        cancel.addActionListener(cl);
    }

    public void addOkListener(ActionListener ol){
        finish.addActionListener(ol);
    }

    public void dispose(){
        frame.dispose();
    }

    public void setStatus(Object n){
        st.setText(n + "");
    }

    public String getStatus(){
        return st.getText();
    }

}
