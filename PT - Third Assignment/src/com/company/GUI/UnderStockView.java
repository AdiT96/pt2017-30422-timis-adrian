package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 24-Apr-17.
 */
public class UnderStockView {

    private JButton ok = new JButton("Ok");
    private JLabel label = new JLabel("The product is UnderStocked");
    private JFrame frame;

    public UnderStockView(){

        frame = new JFrame("UnderStock");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);

        JPanel p1 = new JPanel();
        p1.add(label);
        JPanel p2 = new JPanel();
        p2.add(ok);

        JPanel p = new JPanel();

        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void addOkListener(ActionListener ol){
        ok.addActionListener(ol);
    }

    public void dispose(){
        frame.dispose();
    }



}
