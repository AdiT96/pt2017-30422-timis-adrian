package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 23-Apr-17.
 */
public class UpdateClientView {

    private JButton finish = new JButton("Finish");
    private JButton cancel = new JButton("Cancel");
    private JFrame frame;
    JTextField nt;
    JTextField at;

    public UpdateClientView(){

        frame = new JFrame("Update Client");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);


        JLabel name = new JLabel("Name: ");
        JLabel address = new JLabel("Address: ");

        nt = new JTextField(40);
        at = new JTextField(40);

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();


        p1.add(name);
        p1.add(nt);
        p2.add(address);
        p2.add(at);


        p3.add(cancel);
        p3.add(finish);

        JPanel p =  new JPanel();

        p.add(p1);
        p.add(p2);
        p.add(p3);


        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

    }

    public void addCancelListener(ActionListener cl){
        cancel.addActionListener(cl);
    }

    public void addOkListener(ActionListener ol){
        finish.addActionListener(ol);
    }

    public void dispose(){
        frame.dispose();
    }

    public void setName(Object n){
        nt.setText(n + "");
    }

    public void setAddress(Object n){
        at.setText(n + "");
    }

    public String getName(){
        return nt.getText();
    }

    public String getAddress(){
        return at.getText();
    }

}
