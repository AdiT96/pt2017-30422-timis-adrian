package com.company.BusinessLogic;

import com.company.DAO.*;
import com.company.Model.*;

import java.util.List;

/**
 * Created by Adi on 16-Apr-17.
 */
public class WarehouseAdministration {

    private static ProductDAO p = new ProductDAO();

    public static void insertProduct(Product product){
        p.insert(product);
    }

    public static void removeProductById(int id){
        ProductDAO.removeProductById(id);
    }

    public static List<Product> selectAllProducts(){
        return p.findAll();
    }

    public static Product selectProductById(int id){
        return p.findById(id);
    }

    public static void updateStockById(int id, int stock){
        ProductDAO.updateStock(id, stock);
    }

    public static void updateProduct(Product product){
        p.update(product);
    }

}
