package com.company.BusinessLogic;

import com.company.DAO.*;
import com.company.Model.*;

import java.util.List;

/**
 * Created by Adi on 16-Apr-17.
 */
public class ClientAdministration{


    private static ClientDAO c = new ClientDAO();

    public static void addClient(Client client){
        c.insert(client);

    }

    public static void removeClient(int id){
        ClientDAO.removeClientById(id);
    }

    public static void updateClient(Client client){
        c.update(client);
    }

    public static Client findClientById(Client client){
        return c.findById(client.getId());
    }

    public static List<Client> selectAllClients(){
        return c.findAll();
    }





}
