package com.company.DAO;

import com.company.Connection.ConnectionFactory;
import com.company.Model.Client;
import com.company.Model.Product;

import java.sql.*;
import java.util.*;
import java.util.logging.Logger;


/**
 * Created by Adi on 15-Apr-17.
 */
public class ProductDAO extends AbstractDAO<Product>{

    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private final static String updateStockStatementString = "UPDATE product SET stock=? WHERE id=?";
    private final static String removeProductStatementString = "DELETE FROM product WHERE id=?";



    public static void removeProductById(int productId){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try{
            deleteStatement = dbConnection.prepareStatement(removeProductStatementString);
            deleteStatement.setInt(1, productId);
            deleteStatement.executeUpdate();

        }catch (SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }

    }

    public static void updateStock(int productId, int stock){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;
        ResultSet rs = null;

        try{
            updateStatement = dbConnection.prepareStatement(updateStockStatementString);
            updateStatement.setInt(1, stock);
            updateStatement.setInt(2, productId);

            updateStatement.executeUpdate();

        }catch (SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(rs);
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }


    }

}
