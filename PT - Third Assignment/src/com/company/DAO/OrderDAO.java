package com.company.DAO;

import com.company.Connection.ConnectionFactory;
import com.company.Model.Order;
import com.company.Model.Product;

import java.util.*;
import java.sql.*;

/**
 * Created by Adi on 16-Apr-17.
 */
public class OrderDAO extends AbstractDAO<Order>{

    private final static String findAllStatement = "SELECT client.name as 'name', client.id AS 'client id', product.name as 'product', product.id AS 'product id', order.status, order.idorder, order.quantity FROM store.client INNER JOIN store.product INNER JOIN store.order ON (store.order.client_id = store.client.id and store.order.product_id = store.product.id)";
    private final static String findByClient = "SELECT * FROM store.order WHERE order.client_id=?";
    private final static String findByProduct = "SELECT * FROM store.order WHERE product_id=?";
    private final static String insertStatementString = "INSERT INTO store.order (client_id,product_id,status,quantity) VALUES (?,?,?,?)";
    private final static String updateStatusStatementString = "UPDATE store.order SET status=? WHERE idorder=?";
    private final static String removeOrderStatementString = "DELETE FROM store.order WHERE idorder=?";


    public static void removeOrderById(int orderId){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try{
            deleteStatement = dbConnection.prepareStatement(removeOrderStatementString);
            deleteStatement.setInt(1, orderId);
            deleteStatement.executeUpdate();

        }catch (SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }

    }

    public static void updateStatus(String status, int orderid){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;
        ResultSet rs = null;

        try{
            updateStatement = dbConnection.prepareStatement(updateStatusStatementString);
            updateStatement.setString(1, status);
            updateStatement.setInt(2, orderid);

            updateStatement.executeUpdate();

        }catch (SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(rs);
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }


    }

    public static int insertOrder(Order order){

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        ResultSet rs = null;

        int insertedId = -1;

        try{

            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, order.getClientId());
            insertStatement.setInt(2, order.getProductId());
            insertStatement.setString(3, order.getStatus());
            insertStatement.setInt(4, order.getQuantity());

            insertStatement.executeUpdate();

            rs = insertStatement.getGeneratedKeys();

            if (rs.next()){
                insertedId = rs.getInt(1);
            }



        }catch(SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(rs);
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);

        }

        return insertedId;

    }


    public static List<Order> findByProduct(int productid){
        List<Order> orders = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try{
            findStatement = dbConnection.prepareStatement(findByProduct);
            findStatement.setInt(1, productid);
            rs = findStatement.executeQuery();

            while(rs.next()){
                int id = rs.getInt("idorder");
                int client_id = rs.getInt("client_id");
                int product_id = rs.getInt("product_id");
                String status = rs.getString("status");
                int quantity = rs.getInt("quantity");

                Order order = new Order(client_id, product_id, status, quantity);
                order.setId(id);
                orders.add(order);

            }



        }catch(SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }

        return orders;


    }

    public static List<Order> findByClient(int clientid){
        List<Order> orders = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try{
            findStatement = dbConnection.prepareStatement(findByClient);
            findStatement.setInt(1, clientid);
            rs = findStatement.executeQuery();

            while(rs.next()){
                int id = rs.getInt("idorder");
                int client_id = rs.getInt("client_id");
                int productid = rs.getInt("product_id");
                String status = rs.getString("status");
                int quantity = rs.getInt("quantity");

                Order order = new Order(client_id, productid, status, quantity);
                order.setId(id);
                orders.add(order);

            }



        }catch(SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }

        return orders;


    }

    public static List<Order> selectAllOrders(){

        List<Order> orders = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement selectStatement = null;
        ResultSet rs = null;


        try{
            selectStatement = dbConnection.prepareStatement(findAllStatement);
            rs = selectStatement.executeQuery();

            while(rs.next()){

                int orderId = rs.getInt("idorder");
                int clientId = rs.getInt("client id");
                int productID = rs.getInt("product id");
                String status = rs.getString("status");
                String clientName = rs.getString("name");
                String productName = rs.getString("product");
                int quantity = rs.getInt("quantity");

                Order order = new Order(clientId, productID, status, clientName, productName, quantity);
                order.setId(orderId);

                orders.add(order);

            }

        }catch(SQLException s){
            s.printStackTrace();
        }finally{
            ConnectionFactory.close(rs);
            ConnectionFactory.close(selectStatement);
            ConnectionFactory.close(dbConnection);
        }

        return orders;


    }

}
