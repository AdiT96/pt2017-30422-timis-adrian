package com.company.DAO;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.company.Connection.ConnectionFactory;

/**
 * Created by Adi on 24-Apr-17.
 */
public class AbstractDAO<T> {


    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {

        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append("store.");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    private String createFindAllQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append("store.");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    //"INSERT INTO client (name,address) VALUES (?,?)";

    private String createInsertQuery(T t){

        StringBuilder sb = new StringBuilder("INSERT ");
        sb.append(" INTO ");
        sb.append(" store.");
        sb.append(type.getSimpleName());
        sb.append(" (");
        Field[] fields = type.getDeclaredFields();
        for(int j = 1; j < fields.length; j++){
            sb.append(fields[j].getName());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") VALUES (");


        for(int j = 1; j < fields.length; j++) {

            fields[j].setAccessible(true);

            Object value = null;
            try {
                value = fields[j].get(t);
            } catch (IllegalAccessException i) {
                i.printStackTrace();
            }

            fields[j].setAccessible(false);

            sb.append("'");
            sb.append(value);
            sb.append("'");
            sb.append(",");

        }

        sb.deleteCharAt(sb.length() - 1);

        sb.append(")");

        return sb.toString();

    }

    //"UPDATE client SET name=?,address=? WHERE idclient=?"

    private String createUpdateQuery(T t){

        StringBuilder sb = new StringBuilder("UPDATE ");
        sb.append(" store.");
        sb.append(type.getSimpleName());
        sb.append(" SET ");

        Field[] fields = type.getDeclaredFields();

        for(int j = 1; j < fields.length; j++){

            sb.append(fields[j].getName());
            sb.append("=");

            fields[j].setAccessible(true);
            try {
                sb.append("'");
                sb.append(fields[j].get(t));
                sb.append("'");
            }catch(IllegalAccessException i){
                i.printStackTrace();
            }

            fields[j].setAccessible(false);

            sb.append(",");

        }

        sb.deleteCharAt(sb.length() - 1);

        sb.append(" WHERE ");

        sb.append(fields[0].getName());
        sb.append("='");
        fields[0].setAccessible(true);
        try {
            sb.append(fields[0].get(t));
        }catch(IllegalAccessException i){
            i.printStackTrace();
        }
        fields[0].setAccessible(false);

        sb.append("'");

        return sb.toString();

    }

    public List<T> findAll() {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createFindAllQuery();

        try{

            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);

        }catch(SQLException s){
            LOGGER.log(Level.WARNING, type.getName() + "DA):finaAll " + s.getMessage());
        }finally{
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return null;
    }



    public T update(T t) {

        Connection connection = null;
        PreparedStatement statement = null;

        String query = createUpdateQuery(t);

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);

            statement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return t;
    }

    public T insert(T t) {


        Connection connection = null;
        PreparedStatement statement = null;

        String query = createInsertQuery(t);

        try{
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);

            statement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }




}
