package com.company.Bill;

import com.company.Model.Client;
import com.company.Model.Order;
import com.company.Model.Product;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Adi on 16-Apr-17.
 */
public class BillMaker {

    public void createBill(Client client, Product product, Order order){

        try{
            PrintWriter file = new PrintWriter("Bill-" + client.getName() + "--" + order.getId() +".txt","UTF-8");

            file.println("------------------------------------------------------------------------------------");
            file.println("                                    Bill                                            ");
            file.println("------------------------------------------------------------------------------------");
            file.println("      Client Name        : " + client.getName());
            file.println("      Delivery Address   : " + client.getAddress());
            file.println("------------------------------------------------------------------------------------");
            file.println("      Product            : " + product.getName());
            file.println("      Product Description: " + product.getDescription());
            file.println("      Quantity           : " + order.getQuantity());
            file.println("------------------------------------------------------------------------------------");
            file.println("      Single Price       : " + product.getPrice());
            file.println("      Total Price        : " + product.getPrice() * order.getQuantity());
            file.println("------------------------------------------------------------------------------------");
            DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            Date dateobj = new Date();
            file.println("                                " + df.format(dateobj) + "                          ");
            file.println("------------------------------------------------------------------------------------");
            file.close();
        }catch(IOException e){
            e.printStackTrace();
        }


    }


}
