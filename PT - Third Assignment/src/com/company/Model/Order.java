package com.company.Model;

/**
 * Created by Adi on 15-Apr-17.
 */
public class Order {

    private int id;
    private int clientId;
    private String clientName;
    private int productId;
    private String productName;
    private String status;
    private int quantity;


    public Order(){

    }

    public Order(int clientId, int productId, String status, int quantity) {
        this.clientId = clientId;
        this.productId = productId;
        this.status = status;
        this.quantity = quantity;
    }

    public Order(int clientId, int productId, String status, String clientName, String productName, int quantity) {
        this.clientId = clientId;
        this.productId = productId;
        this.status = status;
        this.clientName = clientName;
        this.productName = productName;
        this.quantity = quantity;
    }

    public int getQuantity(){
        return this.quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString(){
        return status;
    }

}
