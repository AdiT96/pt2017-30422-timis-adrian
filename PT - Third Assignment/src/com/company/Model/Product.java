package com.company.Model;

/**
 * Created by Adi on 15-Apr-17.
 */
public class Product {

    private int id;
    private String name;
    private int stock;
    private String description;
    private int price;

    public Product(){

    }

    public Product(String name, int stock, String description, int price) {
        this.name = name;
        this.stock = stock;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String toString(){
        return name + " -- " + description + " -- " + price + " -- " + stock;
    }
}
