package com.company;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Created by Adi on 07-Mar-17.
 */
public class View extends JFrame{


    private JTextField firstpoly = new JTextField(40);
    private JTextField secondpoly = new JTextField(40);
    private JButton add = new JButton("Add");
    private JButton subtract = new JButton("Subtract");
    private JButton multiply = new JButton("Multiply");
    private JButton divide = new JButton("Divide");
    private JButton differetiantefirst = new JButton("Differentiate First");
    private JButton integratefirst = new JButton("Integrate First");
    private JButton differentiatesecond = new JButton("Differentiate Second");
    private JButton integratesecond = new JButton("Integrate Second");
    private JTextField result = new JTextField(80);
    private JTextField remainder = new JTextField(80);

    private Model m;





    public View(Model mod){

        m = mod;

        JFrame frame = new JFrame("Polynomial Processing!");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080, 720);

        result.setEditable(false);
        remainder.setEditable(false);

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();

        panel1.add(new JLabel("First Polynomial:"));
        panel1.add(firstpoly);
        panel1.add(differetiantefirst);
        panel1.add(integratefirst);

        panel2.add(new JLabel("Second Polynomial:"));
        panel2.add(secondpoly);
        panel2.add(differentiatesecond);
        panel2.add(integratesecond);

        panel3.add(add);
        panel3.add(subtract);
        panel3.add(multiply);
        panel3.add(divide);

        panel4.add(new JLabel("The result is:"));
        panel4.add(result);


        panel5.add(new JLabel("The remainder is:"));
        panel5.add(remainder);

        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.add(panel3);
        p.add(panel4);
        p.add(panel5);
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));


        frame.setContentPane(p);
        frame.setVisible(true);



    }


    public String getFirstPolynomial(){
        return firstpoly.getText();
    }

    public void setFirstPolynomial(String p){
        firstpoly.setText("");
        if (p.isEmpty()){
            firstpoly.setText("0");
        }
        else
            firstpoly.setText(p);
    }

    public String getSecondPolynomial(){
        return secondpoly.getText();
    }

    public void setSecondPolynomial(String p){
        secondpoly.setText("");
        if (p.isEmpty()){
            secondpoly.setText("0");
        }
        else
            secondpoly.setText(p);
    }

    public void setResult(String r){
        result.setText("");
        if (r.isEmpty()){
            result.setText("0");
        }
        else
            result.setText(r);
    }

    public void setRemainder(String r){
        remainder.setText("");
        if (r.isEmpty()){
            remainder.setText("0");
        }
        else
            remainder.setText(r);
    }

    public void addAddListener(ActionListener adl){
        add.addActionListener(adl);
    }

    public void addSubtractListener(ActionListener sul){
        subtract.addActionListener(sul);
    }

    public void addMultiplyListener(ActionListener mul){
        multiply.addActionListener(mul);
    }

    public void addDifferentiateFirstListener(ActionListener dfl){
        differetiantefirst.addActionListener(dfl);
    }

    public void addIntegrateFirstListener(ActionListener ifl){
        integratefirst.addActionListener(ifl);
    }

    public void addDifferentiateSecondListener(ActionListener dsl){
        differentiatesecond.addActionListener(dsl);
    }

    public void addIntegrateSecondListener(ActionListener isl){
        integratesecond.addActionListener(isl);
    }

    public void addDivideListener(ActionListener dil){
        divide.addActionListener(dil);
    }

}
