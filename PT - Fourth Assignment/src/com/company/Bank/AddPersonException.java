package com.company.Bank;

/**
 * Created by Adi on 14-May-17.
 */
public class AddPersonException extends Exception {

    public AddPersonException(String message){

        super(message);

    }

}
