package com.company.Bank;

import com.company.Person.Person;

/**
 * Created by Adi on 14-May-17.
 */
public class Update {

    private Person personToBeUpdated;
    private double moneyWithdrawn;
    private double moneyDeposited;

    public Update(Person personToBeUpdated, double moneyWithdrawn, double moneyDeposited) {
        this.personToBeUpdated = personToBeUpdated;
        this.moneyWithdrawn = moneyWithdrawn;
        this.moneyDeposited = moneyDeposited;
    }

    public Person getPersonToBeUpdated() {
        return personToBeUpdated;
    }

    public double getMoneyWithdrawn() {
        return moneyWithdrawn;
    }

    public double getMoneyDeposited() {
        return moneyDeposited;
    }
}
