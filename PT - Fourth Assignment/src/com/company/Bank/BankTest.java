package com.company.Bank;

import com.company.Accounts.Account;
import com.company.Accounts.DepositException;
import com.company.Accounts.SpendingAccount;
import com.company.Accounts.WithdrawalException;
import com.company.Person.Person;

import static org.junit.Assert.*;

/**
 * Created by Adi on 16-May-17.
 */
public class BankTest {
    @org.junit.Test
    public void withdrawMoney() throws Exception {

        Bank bank  = new Bank();

        Person person = new Person("Cristi", "cris@yahoo.com");
        bank.addPerson(person);
        Account account = new SpendingAccount(234);
        bank.addHolderAssociatedAccount(person, account);

        try {
            bank.withdrawMoney(person, account, 200);
        }catch (WithdrawalException e){
            fail();
        }


    }

    @org.junit.Test
    public void depositMoney() throws Exception {

        Bank bank  = new Bank();

        Person person = new Person("Cristi", "cris@yahoo.com");
        bank.addPerson(person);
        Account account = new SpendingAccount(234);
        bank.addHolderAssociatedAccount(person, account);

        try {
            bank.depositMoney(person, account, 200);
        }catch (DepositException e){
            fail();
        }


    }

    @org.junit.Test
    public void addPerson() throws Exception {

        Bank bank = new Bank();

        Person person = new Person("Mihai Margineanu", "m.marg@yahoo.com");

        try {
            bank.addPerson(person);
        }catch(AddPersonException e){

            fail();

        }


    }

    @org.junit.Test
    public void removePerson() throws Exception {

        Bank bank = new Bank();

        Person person  = new Person("Ciprian" , "cip@yahoo.com");

        bank.addPerson(person);

        try{

            bank.removePerson(person);

        }catch(RemovePersonException e){

            fail();

        }


    }

    @org.junit.Test
    public void addHolderAssociatedAccount() throws Exception {

        Bank bank  = new Bank();

        Person person = new Person("Cristi", "cris@yahoo.com");
        bank.addPerson(person);
        Account account = new SpendingAccount(234);
        try {
            bank.addHolderAssociatedAccount(person, account);
        }catch (HolderException e){
            fail();
        }

    }

    @org.junit.Test
    public void removeHolderAssociatedAccount() throws Exception {

        Bank bank  = new Bank();

        Person person = new Person("Cristi", "cris@yahoo.com");
        bank.addPerson(person);
        Account account = new SpendingAccount(234);
        bank.addHolderAssociatedAccount(person, account);

        try{

            bank.removeHolderAssociatedAccount(person, account);

        }catch (HolderException e){
            fail();
        }


    }

}