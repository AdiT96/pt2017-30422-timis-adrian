package com.company.Bank;

/**
 * Created by Adi on 14-May-17.
 */
public class RemovePersonException extends Exception {

    public RemovePersonException(String message){

        super(message);

    }

}
