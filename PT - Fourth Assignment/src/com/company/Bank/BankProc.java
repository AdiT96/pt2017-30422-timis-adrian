package com.company.Bank;

import com.company.Accounts.Account;
import com.company.Accounts.DepositException;
import com.company.Accounts.WithdrawalException;
import com.company.Person.Person;

/**
 * Created by Adi on 29-Apr-17.
 */
public interface BankProc {

    /**
     *
     * @param person
     * @param account
     * @return
     * @throws WithdrawalException
     *
     * @pre person != null && account != null
     *
     * @post @post account.money == 0
     *
     */
    double withdrawAllMoney(Person person, Account account) throws WithdrawalException;


    /**
     *
     * @param person
     * @param account
     * @param money
     * @throws WithdrawalException
     *
     * @pre account != null && person != null && money > 0
     *
     * @post @pre account.money - money == @post account.money
     *
     */
    double withdrawMoney(Person person, Account account, double money) throws WithdrawalException;

    /**
     *
     * @param person
     * @param account
     * @param money
     * @throws DepositException
     *
     * @pre person != null && account != null && money > 0
     *
     * @post @pre account.money + @param money == @post money
     *
     */
    void depositMoney(Person person, Account account, double money) throws DepositException;


    /**
     *
     * @param person
     * @throws AddPersonException
     *
     * @pre person != null
     * @pre people.contains(person) == false
     *
     * @post @pre people.size() + 1 == @post people.size()
     *
     */
    void addPerson(Person person) throws AddPersonException;


    /**
     *
     * @param person
     * @throws RemovePersonException
     *
     * @pre people.contains(person) == true
     *
     * @post @pre people.size() - 1 == @post people.size()
     *
     */
    void removePerson(Person person) throws RemovePersonException;


    /**
     *
     * @param person
     * @param account
     * @throws HolderException
     *
     * @pre person != null && account != null
     *
     * @post Account list associated to person has incremented its size
     * @post Bucket for person created if it did not exist before
     *
     */
    void addHolderAssociatedAccount(Person person, Account account) throws HolderException;


    /**
     *
     * @param person
     * @param account
     * @throws HolderException
     *
     * @pre person != null && account != null
     * @pre bucket for person key exists
     *
     * @post Account List associated to person has decremented
     *
     */
    void removeHolderAssociatedAccount(Person person, Account account) throws HolderException;


    /**
     *
     * @pre The file "bank.ser" must exist
     *
     * @post populates the Bank class
     *
     */
    void readAccountData();


    /**
     *
     * @pre The file "bank.ser" must exist
     *
     * @post NoChange
     *
     */
    void writeAccountData();



}
