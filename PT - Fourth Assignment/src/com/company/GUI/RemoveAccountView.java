package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 15-May-17.
 */
public class RemoveAccountView {

    JButton ok = new JButton("OK");
    JButton cancel = new JButton("Cancel");
    JFrame frame;

    public RemoveAccountView() {

        frame = new JFrame("Remove Account");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720, 360);

        JLabel label = new JLabel("Are you sure you would like to remove that account?");

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();

        p1.add(label);
        p2.add(ok);
        p2.add(cancel);

        JPanel p = new JPanel();

        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

        addCancelListener(new CancelRemoveClient());

    }

    public class CancelRemoveClient implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {
            dispose();
        }
    }

    public void addCancelListener(ActionListener cl) {
        cancel.addActionListener(cl);
    }

    public void addOkListener(ActionListener ol) {
        ok.addActionListener(ol);
    }

    public void dispose() {
        frame.dispose();
    }

}
