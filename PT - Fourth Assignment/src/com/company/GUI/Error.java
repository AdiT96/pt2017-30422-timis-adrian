package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 16-May-17.
 */
public class Error {

    private JButton ok = new JButton("Ok");
    private JLabel label;
    private JFrame frame;

    public Error(String message){

        frame = new JFrame("Error");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(720,360);

        label = new JLabel(message);

        JPanel p1 = new JPanel();
        p1.add(label);
        JPanel p2 = new JPanel();
        p2.add(ok);

        JPanel p = new JPanel();

        p.add(p1);
        p.add(p2);

        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);

        ok.addActionListener(new OkListener());

    }

    public void addOkListener(ActionListener ol){
        ok.addActionListener(ol);
    }

    public class OkListener implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {
            dispose();
        }
    }

    public void dispose(){
        frame.dispose();
    }

}
