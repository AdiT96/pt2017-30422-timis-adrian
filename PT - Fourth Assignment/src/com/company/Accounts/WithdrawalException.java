package com.company.Accounts;

/**
 * Created by Adi on 14-May-17.
 */
public class WithdrawalException extends Exception {

    public WithdrawalException(String message){

        super(message);

    }

}
