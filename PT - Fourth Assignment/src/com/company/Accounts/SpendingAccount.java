package com.company.Accounts;


import com.company.Accounts.Account;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.UUID;

/**
 * Created by Adi on 29-Apr-17.
 */
public class SpendingAccount extends Account implements Serializable{

    public SpendingAccount(){

        super();

    }

    public SpendingAccount(UUID id){

        super(id);

    }

    public SpendingAccount(double money){

        super(money);

    }

    public String toString(){

        DecimalFormat df = new DecimalFormat("#.00");

        return "The sum available in the account: " + df.format(money);

    }

}
