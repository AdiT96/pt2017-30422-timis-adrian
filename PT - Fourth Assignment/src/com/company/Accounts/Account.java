package com.company.Accounts;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Adi on 29-Apr-17.
 */
public class Account implements Serializable{

    protected double money;
    private UUID id;

    public Account(UUID id){

        this.id = id;

    }

    public Account(){

        this.money = 0;
        id = UUID.randomUUID();

    }

    public Account(double money){

        this.money = money;
        id = UUID.randomUUID();

    }

    public UUID getId() {
        return id;
    }

    public void depositMoney(double depositAmount) throws DepositException{

        if(depositAmount > 0){

            this.money += depositAmount;

        }else{

            throw new DepositException("You cannot deposit negative money");

        }


    }

    public double withdrawMoney(double withdrawAmount) throws WithdrawalException{

        if (this.money - withdrawAmount >= 0){

            this.money -= withdrawAmount;

            return withdrawAmount;

        }else{

            System.out.println("There is not enough money in the account for this operation!");

            throw new WithdrawalException("There is not enough money in the account for this operation");

        }

    }

    public double withdrawMoney() throws WithdrawalException{

        if(this.money > 0){

            double aux = this.money;

            this.money = 0;

            return aux;

        }else{

            throw new WithdrawalException("There is no money to be withdrawn");

        }

    }

    public double getMoney() {
        return money;
    }

    public boolean equals(Object o){

        if(o instanceof Account) {

            return id.equals(((Account) o).getId());

        }else{

            return false;

        }

    }

}
