package com.company;

import java.util.Comparator;

/**
 * Created by Adi on 21-Mar-17.
 */
public class QueueComparator implements Comparator<Queue> {
    public int compare(Queue q1, Queue q2){
        return Integer.compare(q1.size(), q2.size());
    }

}
