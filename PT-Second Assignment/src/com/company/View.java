package com.company;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by Adi on 03-Apr-17.
 */
public class View extends JFrame{


    private JTextField minArrivalTime = new JTextField(10);
    private JTextField maxArrivalTime = new JTextField(10);
    private JTextField minServiceTime = new JTextField(10);
    private JTextField maxServiceTime = new JTextField(10);
    private JTextField nrOfQueues = new JTextField(10);
    private JTextField simTime = new JTextField(10);
    private JTextArea queueEvolution = new JTextArea(10, 80);

    private JButton start = new JButton("Start Simulation!");
    private JButton stop = new JButton("Stop Simulation!");

    private Model m;


    public class TextAreaOutputStream extends OutputStream{


        @Override
        public void write(int b) throws IOException {
            //updateTextArea(String.valueOf((char) b));
            queueEvolution.append(String.valueOf((char) b));
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException{
            queueEvolution.append(new String(b, off, len));
        }

        @Override
        public void write(byte[] b) throws IOException{
            write(b, 0, b.length);
        }

        public void clear(){
            queueEvolution.setText("");
        }
    }



    public View(Model mod){


        TextAreaOutputStream ta = new TextAreaOutputStream();

        System.setOut(new PrintStream(ta, true));

        m = mod;

        JFrame frame = new JFrame("Queue Simulation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080,720);

        queueEvolution.setEditable(false);


        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel31 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();

        panel1.add(new JLabel("Minimum Arrival Time:"));
        panel1.add(minArrivalTime);
        panel1.add(new JLabel("Maximum Arrival Time:"));
        panel1.add(maxArrivalTime);

        panel2.add(new JLabel("Minimum Service Time:"));
        panel2.add(minServiceTime);
        panel2.add(new JLabel("Maximum Service Time:"));
        panel2.add(maxServiceTime);

        panel3.add(new JLabel("Number of Queues:"));
        panel3.add(nrOfQueues);
        panel3.add(new JLabel("Simulation time (in hours):"));
        panel3.add(simTime);

        panel31.add(start);
        panel31.add(stop);

        panel4.add(new JLabel("Queue Evolution:"));

        panel5.add(queueEvolution);

        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.add(panel3);
        p.add(panel31);
        p.add(panel4);
        p.add(panel5);
        p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

        frame.setContentPane(p);
        frame.setVisible(true);



    }

    public String getSimulationTime(){
        return simTime.getText();
    }

    public String getMinimumArrivalTime(){
        return minArrivalTime.getText();
    }

    public String getMaximumArrivalTime(){
        return maxArrivalTime.getText();
    }

    public String getMinimumServiceTime(){
        return minServiceTime.getText();
    }

    public String getMaximumServiceTime(){
        return maxServiceTime.getText();
    }

    public String getNumberOfQueues(){
        return nrOfQueues.getText();
    }

    public void clearQueues(){
        queueEvolution.setText("");
    }

    public void addStartListener(ActionListener sl){
        start.addActionListener(sl);
    }

    public void addStopListener(ActionListener stl){
        stop.addActionListener(stl);
    }





}
