package com.company;

/**
 * Created by Adi on 19-Mar-17.
 */
public class Customer {

    private int serviceTime;
    private int timeToBeServiced;
    private int waitingTime;
    private int timeArrivalAtQueue;


    /*
    Initializing the customer, with a given arrival and service time, which are random, producem in the customer factory
     */
    public Customer(int service, int arrival){
        this.serviceTime = service;
        this.timeToBeServiced = arrival;
        this.waitingTime = 0;

    }


    /*
    Sets the arrival time at the queue for this specific customer
    this timeArrivalAtQueue is the time in minutes, when the customer will be added to the queue
     */
    public void setTimeArrivalAtQueue(int ta){
        this.timeArrivalAtQueue = ta + timeToBeServiced;
    }



    public void setTimeToBeServiced(int t){
        this.timeToBeServiced = t;
    }

    //Decrements the time to be serviced for this specific customer, i.e. the time the customer will join the queue
    public void decrementTimeToBeServiced(){
        this.timeToBeServiced -= 1;
    }

    //Decrements the service time for the customer, this method is called in the service customer method in the threads
    public void decrementServiceTime(){
        this.serviceTime -= 1;
    }

    //This method increments the wating time for a customer when the customer is in a queue but is not serviced
    public void incrementWaitingTime(){
        this.waitingTime++;
    }

    public int getWaitingTime(){
        return this.waitingTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public int getTimeToBeServiced(){
        return this.timeToBeServiced;
    }

    public String toString(){
        return String.format("%d %02d:%02d", this.serviceTime - 1, this.timeArrivalAtQueue/60, this.timeArrivalAtQueue%60);
    }


}
