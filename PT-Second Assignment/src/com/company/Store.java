package com.company;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Adi on 28-Mar-17.
 */

public class Store implements Runnable{

    private List<Customer> customersInTheStore;
    private List<Queue> queues;
    private List<Thread> threads;
    private int nrOfQueues;
    private int minServiceTime;
    private int maxServiceTime;
    private int minArrivalTime;
    private int maxArrivalTime;
    private int timeOpened = 0;
    private View v;
    private int simulationTime;
    private boolean open;
    private int[] peakHours;

    public Store(int minA, int maxA, int minS, int maxS, int q, View vi, int s){
        customersInTheStore = new ArrayList<Customer>();
        queues = new ArrayList<Queue>();
        threads = new ArrayList<Thread>();
        this.minArrivalTime = minA;
        this.maxArrivalTime = maxA;
        this.minServiceTime = minS;
        this.maxServiceTime = maxS;
        this.nrOfQueues = q;
        this.v = vi;
        this.simulationTime = s;
        this.peakHours = new int[s];
    }

    /*
    This method returns the index of the smallest queue based on the number of customers present in the queue
     */
    private int getSmallestQueue(){
        int smallest = queues.get(0).size();
        int index = 0;
        for (int i = 1; i <= queues.size() - 1; i++){
            if (queues.get(i).size() < smallest){
                smallest = queues.get(i).size();
                index = i;
            }
        }

        return index;
    }

    /*
    This method return the hour when the most amount of customers entered the store.
    It does this by getting the index of the element with the maximum in the array peakHours
    that array stores the amoun of customers that entered the store by the hour
     */
    public int getPeakHour(){
        int max = 0;
        for (int i =1; i < peakHours.length; i++){
            if (peakHours[i] > peakHours[max]) max = i;
        }

        return max;
    }

    /*
    This method terminates the simulation, by changing the boolean variable on which the queue threads
    are running
     */
    public void terminateSimulation(){
        for (Queue q : queues){
            q.terminate();
            open = false;
        }
    }



    /*
    This is the method overridden from the runnable interface
    It is necessary for this method to be a thread as well, because otherwise the GUI would freeze
     */
    public void run(){


        CustomerFactory cf = new CustomerFactory(minServiceTime, maxServiceTime, minArrivalTime, maxArrivalTime);
        //Here we generate a new CustomerFactory, which we use to generate new customers
        PrintStream out = null;
        try {
            out = new PrintStream(new FileOutputStream("Logger.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Here we start initialize all the queues
        for (int i = 0; i < nrOfQueues; i++){
            queues.add(new Queue(out));
            threads.add(new Thread(queues.get(i)));
        }

        out.println("The simulation has started!");


        open = true;
        boolean generateCustomers = true;
        int smallestIndex = 0;


        //This is where we start all the threads
        for (Thread t : threads){
            t.start();
        }

        //The simulation runs on a while loop, based on the boolean variable open, which means that the store is still running
        while (open){

            try{
                //We first sleep this thread to simulate a second has passed, which is actually a minute in the simulation
                Thread.sleep(1000);
                //We use the method v.clearQueues() because we need to set the text area to be empty
                v.clearQueues();
            }catch(InterruptedException e){
                System.out.println("The main thread was interrupted!");
            }


            //If we still generate a new customer, then we enter this loop
            if (generateCustomers) {
                /*
                I considere to generate a customer at the start at the simulation, and whenever the minimum arrival time has passed
                This is why this if with 2 conditions is used
                 */
                if (timeOpened % minArrivalTime == 0 || timeOpened == 0) {

                    //We generate a new customer
                    Customer c = cf.generateCustomer();
                    //We set the arrival time to the real time in the simulation
                    c.setTimeArrivalAtQueue(timeOpened);
                    //We add it as a customer in the store
                    customersInTheStore.add(c);
                    out.println("Customer added in the store");
                    //We sort the customers based on their arrival time
                    Collections.sort(customersInTheStore, new CustomerComparator());

                    /*
                    We use an auxiliary list so that we cand go through all the customers in the store, and delete the ones
                    that need to be placed in a queue
                     */
                    List<Customer> aux = new ArrayList<>();
                    aux.addAll(customersInTheStore);


                    for (Customer customer : aux) {
                        //We compute the index of the smallest queue based on the number of people in that queue
                        smallestIndex = getSmallestQueue();
                        if (customer.getTimeToBeServiced() == 0) {
                            //If we found a customer which needs to be placed in a queue, we add it to the queue
                            //with the least number of customers
                            queues.get(smallestIndex).addCustomer(customer);
                            out.println("Customer added in the queue " + smallestIndex);
                            //We count a new customer added in this hour
                            peakHours[timeOpened/60]++;
                            //We compute the average service time
                            queues.get(smallestIndex).computeAverageServiceTime(customer.getServiceTime());
                            //We remove it from the customers currently in the store
                            customersInTheStore.remove(customer);
                        }
                        //For all the others whose time to be in a queue has not reached yet, we simply
                        //decrement their time to be serviced
                        customer.decrementTimeToBeServiced();
                    }
                }
            }
            else{
                System.out.println("Serving customers after closing time!");
            }

            //Here we print the state of the queues
            System.out.println("");
            System.out.println(String.format("Current time: %02d:%02d", timeOpened/60, timeOpened%60));
            for (Queue q : queues){
                System.out.print("Queue " + queues.indexOf(q) + ": ");
                System.out.println(q);
            }




            //We add a minute to the time the store was opened
            timeOpened ++ ;

            //If the time has passed the simulation time, then we enter in this loop
            if (timeOpened >= simulationTime * 60){
                //We set open and generate customers as false
                open = false;
                generateCustomers = false;

                //We check if a queue still has some customers in it, and then, we change open to true
                //because we still want to service the customers who are waiting in the queues
                for (Queue q : queues){
                    if (!(q.checkIfEmpty())) {
                        open = true;
                    }
                }

                //If open was not changed to true, it means that there are no customers waiting in the queue
                //so we terminate all the queue threads
                if (!open){
                    for (Queue q : queues){
                        q.terminate();
                    }
                }
            }

        }

        //Here we print the information after the simulation
        for (Queue q : queues){
            System.out.println("The average waiting time for queue " + queues.indexOf(q) + " is: " + q.getAverageWaitingTime());
            System.out.println("The average service time for queue " + queues.indexOf(q) + " is: " + q.getAverageServiceTime());
            System.out.println("The time queue " + queues.indexOf(q) + " spent empty is: " + String.format("%02d:%02d", q.getEmptyTime()/60, q.getEmptyTime()%60));
            System.out.println("Customers served in total in queue " + queues.indexOf(q) + " is: " + q.getCustomersServed());
        }

        System.out.println("The peak hour was hour: " + getPeakHour());


    }


}
