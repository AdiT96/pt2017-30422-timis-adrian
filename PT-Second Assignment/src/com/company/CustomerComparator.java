package com.company;

import java.util.Comparator;

/**
 * Created by Adi on 19-Mar-17.
 */
public class CustomerComparator implements Comparator<Customer>{

    public int compare(Customer c1, Customer c2){
        return Integer.compare(c1.getServiceTime(), c2.getServiceTime());
    }

}
