package com.company;

import java.io.PrintStream;
import java.util.*;

/**
 * Created by Adi on 19-Mar-17.
 */
public class Queue implements Runnable{

    private List<Customer> customerQueue;
    private float averageWaitingTime;
    private float averageServiceTime;
    private int customersServed;
    private boolean running;
    private PrintStream out;
    private int emptyTime;

    public Queue(PrintStream o){
        customerQueue = new LinkedList<Customer>();
        averageWaitingTime = 0;
        averageServiceTime = 0;
        customersServed = 0;
        running = true;
        out = o;
        emptyTime = 0;
    }

    public int size(){return customerQueue.size();}

    /*
    Adds a customer to the end of the list
     */
    public void addCustomer(Customer c){
        this.customerQueue.add(c);
    }



    public String toString(){
        String s = "";
        for (int i = 0; i < customerQueue.size(); i++){
            s += "C" + i + " " + customerQueue.get(i) + " ";
        }
        if (this.checkIfEmpty()){
            return "The queue is empty!";
        }
        return s;
    }

    //This method computes the average waiting time for this queue after this simulation is complete
    public void computeAverageWaitingTime(int waitingTime){
        averageWaitingTime = ((customersServed * averageWaitingTime) + waitingTime)/(customersServed + 1);

    }

    //This method computes the average waiting time for this queue after the simulation has ended
    public float getAverageWaitingTime(){
        return averageWaitingTime;
    }

    //This method computes the sum of all the service times in this particular queue
    public void computeAverageServiceTime(int serviceTime){
        averageServiceTime += serviceTime;
    }

    //This method return the average service time for this queue
    public float getAverageServiceTime(){
        return averageServiceTime / customersServed;
    }

    /*
    This is the main method where the first customer in the queue is serviced, this is done by decrementing
    the the service time of the first customer, and if that customer has the service time equal to 0,
    it removes that customer from the list
     */
    public void serviceCustomer(){
        if (!customerQueue.isEmpty()) {
            customerQueue.get(0).decrementServiceTime();
            if (customerQueue.get(0).getServiceTime() == 0) {
                computeAverageWaitingTime(customerQueue.get(0).getWaitingTime());
                customerQueue.remove(0);
                out.println("Customer was removed from queue");
                customersServed++;
            }

        }else{
            emptyTime ++;
        }

    }

    public boolean checkIfEmpty(){
        return customerQueue.isEmpty();
    }

    public int getEmptyTime() {
        return emptyTime;
    }

    //This method changes the boolean variable on which the loop inside the run method is based
    public void terminate(){
        this.running = false;
    }

    public int getCustomersServed(){
        return customersServed;
    }

    /*
    This method, which is overridden from the Runnable interface. It simply sleeps for one second, and then
    services the first customer, and increments the waiting time for all the others that are not at the start
    of the queue
     */
    public void run(){

        while (running) {

            try {
                Thread.sleep(1000);
            }catch (InterruptedException e){
                System.out.println("The thread was interrupted!");
            }

            this.serviceCustomer();
            for (int i = 1; i < customerQueue.size(); i++){
                customerQueue.get(i).incrementWaitingTime();
            }
        }


    }



}
