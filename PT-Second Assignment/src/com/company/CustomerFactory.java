package com.company;

/**
 * Created by Adi on 19-Mar-17.
 */
public class CustomerFactory {

    private int minimumServiceTime;
    private int maximumServiceTime;
    private int minimumArrivalTime;
    private int maximumArrivalTime;

    public CustomerFactory(int mins, int maxs, int mina, int maxa){
        this.minimumServiceTime = mins;
        this.maximumServiceTime = maxs;
        this.minimumArrivalTime = mina;
        this.maximumArrivalTime = maxa;
    }

    /*
    Generates a new customer, with a random arrival and service time
     */
    public Customer generateCustomer(){
        return new Customer((int)(Math.random() * (this.maximumServiceTime - this.minimumServiceTime) + this.minimumServiceTime),
                            (int)(Math.random() * (this.maximumArrivalTime - this.minimumArrivalTime) + this.minimumArrivalTime));
    }

}
